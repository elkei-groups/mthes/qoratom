from setuptools import setup, find_packages

with open("README.md") as f:
    readmeText = f.read()

with open('LICENSE') as f:
    licenseText = f.read()

setup(
    name='qoratom',
    version='0.1.0',
    description='Tool that replaces simple QoS contract specifications with corresponding rankings',
    long_description=readmeText,
    long_description_content_type="text/markdown",
    author='Elias Keis',
    author_email='git-commits@elkei.de',
    url='https://gitlab.com/elkei-groups/mthes/qoratom',
    license=licenseText,
    packages=find_packages(exclude=['tests', 'tests.*', 'docs']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['qoratom=qoratom.app:run'],
    },
    install_requires=[
        'antlr4-python3-runtime'
    ],
    tests_require=[
        'pytest'
    ],
)
