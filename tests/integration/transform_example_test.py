import importlib.resources
import pathlib
from typing import Dict

import pytest

import tests.integration
from qoratom.domain.formula import Formula
from qoratom.domain.ranking import Ranking
from qoratom.ranking_transformer import RankingTransformer


@pytest.fixture()
def mock_ranking_executor():
    class MockRankingExecutor:
        def cleanup(self):
            pass

        # noinspection PyMethodMayBeStatic
        def execute(self, ranking: Ranking) -> Dict[Formula, Dict[Formula, float]]:
            return {
                rq: {
                    pr: len(str(rq)) / (len(str(rq)) + len(str(pr))) for pr in ranking.provisions
                } for rq in ranking.requirements
            }

    return MockRankingExecutor()


def test_transform_example(mock_ranking_executor, tmp_path: pathlib.Path):
    # given an usual example input file
    with importlib.resources.path(tests.integration, "example.mzn") as infile:
        # when executing the transformer mocking the MiniZinc/PolyRank executions
        outfile = tmp_path / "example_out.mzn"
        RankingTransformer(str(infile), str(outfile), mock_ranking_executor).transform()
    # then the output should match the expected result
    expectation = importlib.resources.read_text(tests.integration, "example_out.mzn")
    assert outfile.read_text() == expectation
