set of int: PROV = 1..2;
set of int: REQ = 1..3;
set of float: RANK = 0.0..1.0;

% first example with only one requirement
array[{1}, PROV] of RANK: ranks1 = ContractRanks {
  requirement: [0 ≤ cost ≤ 2 ∧ 0 ≤ latency ≤ 2];
  provisions:  [1 ≤ cost ≤ 3 ∧ 1 ≤ latency ≤ 3,
                2 ≤ cost + latency ≤ 4 ∧ cost ≤ latency + 2 ∧ 2 + cost ≥ latency];
};

% new example with multiple requirements
array[REQ, PROV] of RANK: ranks2 = ContractRanks {
  % provisions stay the same
  provisions: [1 ≤ cost ≤ 3 ∧ 1 ≤ latency ≤ 3,
               2 ≤ cost + latency ≤ 4 ∧ cost ≤ latency + 2 ∧ 2 + cost ≥ latency,
  ];
  % two additional requirements
  requirements: [0 ≤ cost ≤ 2 ∧ 0 ≤ latency ≤ 2, % Rq1 stays same
                 1 ≤ cost ≤ 3 ∧ 0 ≤ latency ≤ 2,
                 0 ≤ cost ≤ 3 ∧ 0 ≤ latency ≤ 2,
  ];
};


% parameterized example
array[{1}, PROV] of RANK: ranks3 = ContractRanks {
  params: {
    var 0.0..2.0: shift;
    % choose shift so that both provisions should be ranked equally
    constraint (2 - shift) * (2 - shift) / (2*2) = 0.5;
  };
  requirement: [0 ≤ cost ≤ 2 ∧ 0 ≤ latency ≤ 2];
  provisions:  [0 + _shift ≤ cost ≤ 2 + _shift ∧ 0 + _shift ≤ latency ≤ 2 + _shift,
                2 ≤ cost + latency ≤ 4 ∧ cost ≤ latency + 2 ∧ 2 + cost ≥ latency];
};

var PROV: p;
solve maximize(ranks1[1, p] + sum(r in REQ)(ranks2[r, p]) + ranks3[1, p]);
