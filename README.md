# QoraTom

A small python tool that replaces simple QoS contract specifications in MiniZinc/-Brass by the corresponding ranking from Agustíns QoS ranking tool.

## Requirements

- Python 3
- [ANTLR4](https://www.antlr.org/download.html)
- Agustíns polytope ranking tool (callable as `polyrank`)
- for parameterized contracts: [MiniZinc](https//www.minizinc.org) (callable as `minizinc`)

## Usage

Before you can use the tool, you have to generate the parser files:
```sh
cd qoratom/parser
antlr4 -Dlanguage=Python3 -o generated QosContractLexer.g4
antlr4 -Dlanguage=Python3 -o generated -visitor -listener QosContractParser.g4
```

You can then run the tool and pass input and output file name as parameters:
```sh
pip install -e .
qoratom example.mzn example_out.mzn
```
If you omit the output file name, the output will be written to a file with the name as the input name but "_out" added at the end.
Run `qoratom -h` for the complete usage syntax.