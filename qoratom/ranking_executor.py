import pathlib
import re
import shutil
import subprocess
from tempfile import mkdtemp
from typing import Dict, List, Optional

from qoratom.domain.formula import Formula
from qoratom.domain.ranking import Ranking
from qoratom.minizinc_executor import MinizincExecutor
from qoratom.poly_generator import PolyGenerator


class RankingExecutor:
    def __init__(self, mzn_exec: MinizincExecutor, ranker_executable: str = "polyrank", debug: bool = False):
        self.mzn_exec = mzn_exec
        self.ranker_executable = ranker_executable
        self.debug = debug
        self.temp_dir: Optional[str] = None

    def __enter__(self):
        return self

    def __exit__(self, exc, value, tb):
        if self.debug:
            if self.temp_dir:
                print(f"Warning: temporary directory {self.temp_dir} will not be removed!")
            return
        self.cleanup()

    def cleanup(self):
        if self.temp_dir:
            shutil.rmtree(self.temp_dir)

    def execute(self, ranking: Ranking) -> Dict[Formula, Dict[Formula, float]]:
        if self.temp_dir is None:
            self.temp_dir = mkdtemp(prefix="qoratom_rankexec_")

        # evaluate parameters
        if ranking.params_problem:
            param_values = self.mzn_exec.solve(ranking.params_problem, ranking.get_parameters())
        else:
            param_values = dict()

        # generate poly files
        polygen = PolyGenerator(ranking.get_identifiers(), param_values)
        requirement_files = self._generate_poly_files(polygen, "Rq", ranking.requirements)
        provision_files = self._generate_poly_files(polygen, "Pr", ranking.provisions)

        # do the ranking
        return {
            rq: {
                pr: self._call_ranker(requirement_files[rq], provision_files[pr]) for pr in ranking.provisions
            } for rq in ranking.requirements
        }

    def _generate_poly_files(self, polygen: PolyGenerator, prefix: str, formulas: List[Formula]) -> \
            Dict[Formula, pathlib.Path]:
        filenames = dict()
        for i, formula in enumerate(formulas):
            file = pathlib.Path(self.temp_dir, f"{prefix}-{i}")
            polygen.generate_to_file(formula, file)
            if self.debug:
                print(f"Generated poly ranker input file: {file}\n\t↳ formula: {formula}")
            filenames[formula] = pathlib.Path(file)
        return filenames

    def _call_ranker(self, req_file: pathlib.Path, prov_file: pathlib.Path) -> float:
        command = [self.ranker_executable, prov_file, req_file]
        # noinspection PyTypeChecker
        proc: subprocess.CompletedProcess = subprocess.run(command, text=True, stdout=subprocess.PIPE,
                                                           stderr=subprocess.PIPE, check=True)
        expected_output_match = re.match("^Ranking: ([0-9]+(\\.[0-9]+)?)\n?$", proc.stdout)
        if expected_output_match and proc.stderr == "":
            return float(expected_output_match.group(1))

        if proc.stderr == "Error trying to run PolyVest.\nError message:  b\'\\nerror: chol(): decomposition " \
                          "failed\\n\'\n" or proc.stdout.startswith("Ranking: inf"):
            # PolyRank fails with this error message or returns inf whenever the intersection is empty
            if self.debug:
                print(f"Interpreting poly ranker output for {req_file.stem}, {prov_file.stem} as 0 result\n"
                      f"\t↳ stdout: \"{proc.stdout}\"\n"
                      f"\t↳ sterr:  \"{proc.stderr}\"")
            return 0.0
        raise RuntimeError("RankerTool returned unexpected output", proc.stdout, proc.stderr)
