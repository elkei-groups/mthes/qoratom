import os
import random
import shutil
import string
import subprocess
import tempfile
from typing import Set, Dict, Optional


class MinizincExecutor:
    _solution_output_start = "=== SOLUTIONS START ==="
    _solution_output_end = "===  SOLUTIONS END  ==="

    def __init__(self, mzn_executable: str = "minizinc", mzn_args: Optional[str] = None, debug: bool = False):
        self.mzn_exec = mzn_executable
        self.mzn_args = mzn_args
        self.debug = debug
        self.temp_dir: Optional[str] = None

    def solve(self, problem: str, solution_vars: Set[str]) -> Dict[str, float]:
        if self.temp_dir is None:
            self.temp_dir = tempfile.mkdtemp(prefix="qoratom_mznexec_")
        filename = self._generate_mzn_file(problem, solution_vars)
        if self.debug:
            print(f"Generated MiniZinc file: {filename}")
        output = self._call_minizinc(filename)
        solution = self._parse_output(output)
        if self.debug:
            print(f"MiniZinc solution: {solution}")
        return solution

    def _generate_mzn_file(self, problem: str, solution_vars: Set[str]):
        random_name = ''.join(random.choice(string.ascii_letters) for _ in range(10))
        filename = os.path.join(self.temp_dir, f"{random_name}.mzn")
        with open(filename, 'w') as file:
            file.write(problem)
            file.write(self._generate_output_str(solution_vars))
        return filename

    @staticmethod
    def _generate_output_str(solution_vars: Set[str]) -> str:
        variable_output: str = "\\n".join(f"{var}=\\({var})" for var in solution_vars)
        return f"\noutput [\"{MinizincExecutor._solution_output_start}" \
               f"\\n{variable_output}" \
               f"\\n{MinizincExecutor._solution_output_end}\"] "

    def _call_minizinc(self, file: str) -> str:
        command = [self.mzn_exec] + (self.mzn_args.split(" ") if self.mzn_args is not None else []) + [file]
        try:
            proc: subprocess.CompletedProcess = subprocess.run(command, text=True, stdout=subprocess.PIPE,
                                                               stderr=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError as e:
            raise RuntimeError(str(e), e.stderr)
        if proc.stderr != "":
            raise RuntimeError("MiniZinc wrote to stderr", proc.stderr)
        return proc.stdout

    @staticmethod
    def _parse_output(output: str) -> Dict[str, float]:
        # find final output
        start = output.rfind(MinizincExecutor._solution_output_start)
        if start < 0:
            raise RuntimeError("MiniZinc output did not contain 'start of output' marker")
        start += len(MinizincExecutor._solution_output_start)
        end = output.find(MinizincExecutor._solution_output_end, start)
        if end < 0:
            raise RuntimeError("MiniZinc output did not contain 'end of output' marker")
        output = output[start:end].strip()

        # extract variable values
        result = dict()
        for line in output.splitlines():
            [varname, value] = line.split("=", 1)
            result[varname.strip()] = float(value.strip())
        return result

    def __enter__(self):
        return self

    def __exit__(self, exc, value, tb):
        if self.debug:
            if self.temp_dir:
                print(f"Warning: temporary directory {self.temp_dir} will not be removed!")
            return
        self.cleanup()

    def cleanup(self):
        if self.temp_dir:
            shutil.rmtree(self.temp_dir)
