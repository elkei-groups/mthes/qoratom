import pathlib
import sys
from typing import Set, List, TextIO, Dict

from qoratom.domain.clause import Clause
from qoratom.domain.flat.flat_clause import FlatClause
from qoratom.domain.flat.flat_geq_0_constraint import FlatGeq0Constraint
from qoratom.domain.formula import Formula


class PolyGenerator:
    def __init__(self, identifiers: Set[str], param_values: Dict[str, float]):
        self.identifiers = list(identifiers)
        self.identifiers.sort()
        self.param_values = param_values

    def generate_to_file(self, formula: Formula, file: pathlib.Path):
        with open(file, "w") as file:
            self.generate(formula, file)

    def generate_to_stdout(self, formula: Formula):
        self.generate(formula, sys.stdout)

    def generate(self, formula: Formula, output: TextIO):
        PolyGeneratorWithOutput(self.identifiers, output, self.param_values).generate(formula)


class PolyGeneratorWithOutput:
    def __init__(self, identifiers: List[str], output: TextIO, param_values: Dict[str, float]):
        self.identifiers = identifiers
        self.output = output
        self.param_values = param_values

    def generate(self, formula: Formula):
        print(len(formula.clauses), file=self.output)
        for clause in formula.clauses:
            self._generate_clause(clause)

    def _generate_clause(self, clause: Clause):
        flat_clause: FlatClause = clause.flatten(self.param_values)
        print(f"{len(flat_clause.constraints)} {len(self.identifiers)}", file=self.output)
        for constraint in flat_clause.constraints:
            self._generate_constraint(constraint)

    def _generate_constraint(self, constraint: FlatGeq0Constraint):
        numbers = [constraint.get_constant()] + [constraint.get_coefficient_by_id(i) for i in self.identifiers]
        print(" ".join([str(number) for number in numbers]), file=self.output)
