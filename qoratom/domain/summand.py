from typing import Optional

from .coefficient import Coefficient


class Summand:
    def __init__(self, coefficient: Coefficient, identifier: Optional[str]):
        self.coefficient = coefficient
        self.identifier = identifier

    def get_parameters(self):
        return self.coefficient.get_distinct_parameters()

    def invert(self):
        self.coefficient = -self.coefficient

    def __neg__(self):
        return Summand(-self.coefficient, self.identifier)

    def __str__(self):
        return f"{self.coefficient} * {self.identifier}" if self.identifier else self.coefficient.__str__()
