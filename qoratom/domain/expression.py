from typing import Set, List, Dict

from .summand import Summand


class Expression(object):
    def __init__(self, summands: List[Summand]):
        self.summands = summands

    def get_identifiers(self) -> Set[str]:
        return {summand.identifier for summand in self.summands if summand.identifier is not None}

    def get_parameters(self) -> Set[str]:
        return {param for summand in self.summands for param in summand.get_parameters()}

    def evaluate_coefficients(self, param_values: Dict[str, float]) -> Dict[str, float]:
        by_id: Dict[str, float] = {}
        for summand in self.summands:
            ident = summand.identifier
            val = summand.coefficient.evaluate(param_values)
            if ident in by_id:
                by_id[ident] += val
            else:
                by_id[ident] = val
        return by_id

    def __str__(self):
        return " + ".join([f"({summand})" for summand in self.summands])
