from typing import Set, List, Optional, Dict

from .expression import Expression
from .flat.flat_geq_0_constraint import FlatGeq0Constraint
from .relation import Relation


class Constraint(object):
    def __init__(self, lexpression: Expression, relation: Relation, rexpression: Optional[Expression] = None):
        self.lexpression: Expression = lexpression
        self.relation: Relation = relation
        self.rexpression: Optional[Expression] = rexpression

    def get_identifiers(self) -> Set[str]:
        return self.lexpression.get_identifiers().union(self.rexpression.get_identifiers())

    def get_parameters(self) -> Set[str]:
        return self.lexpression.get_parameters().union(self.rexpression.get_parameters())

    def flatten(self, param_values: Dict[str, float]) -> List[FlatGeq0Constraint]:
        right_coefficients = self.rexpression.evaluate_coefficients(param_values)
        left_coefficients = self.lexpression.evaluate_coefficients(param_values)

        def to_flat_constraint(pos_coef: Dict[str, float], neg_coef: Dict[str, float]):
            return FlatGeq0Constraint({key: pos_coef.get(key, 0) - neg_coef.get(key, 0)
                                       for key in set.union(set(), pos_coef.keys(), neg_coef.keys())})

        if self.relation == Relation.EQ:
            return [
                to_flat_constraint(left_coefficients, right_coefficients),
                to_flat_constraint(right_coefficients, left_coefficients)
            ]
        elif self.relation == Relation.LEQ:
            return [to_flat_constraint(right_coefficients, left_coefficients)]
        else:
            return [to_flat_constraint(left_coefficients, right_coefficients)]

    def __str__(self):
        return f"{self.lexpression} {self.relation} {self.rexpression}"
