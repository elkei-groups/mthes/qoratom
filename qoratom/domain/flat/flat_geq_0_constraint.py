from typing import Dict, Optional


class FlatGeq0Constraint:
    def __init__(self, coefficients_by_id: Dict[Optional[str], float]):
        self._coefficients_by_id = coefficients_by_id

    def __str__(self):
        text = str(self.get_constant())
        for identifier in (self._coefficients_by_id.keys()):
            if identifier is None:
                continue
            value = self.get_coefficient_by_id(identifier)
            text += f" {'-' if value < 0 else '+'} {abs(value)} * {identifier} "
        return text + " ≥ 0"

    def get_coefficient_by_id(self, identifier: Optional[str]):
        return self._coefficients_by_id[identifier] if identifier in self._coefficients_by_id else 0

    def get_constant(self):
        return self.get_coefficient_by_id(None)
