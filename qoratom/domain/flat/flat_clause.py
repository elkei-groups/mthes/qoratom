from typing import List

from .flat_geq_0_constraint import FlatGeq0Constraint


class FlatClause:

    def __init__(self, constraints: List[FlatGeq0Constraint]):
        self.constraints = constraints

    def __str__(self):
        return " ∧ ".join([str(constr) for constr in self.constraints])
