from typing import List, Set, Callable, TypeVar, Iterable, Optional

from .formula import Formula


class Ranking:
    def __init__(self, requirements: List[Formula], provisions: List[Formula], params_problem: Optional[str] = None):
        self.requirements = requirements
        self.provisions = provisions
        self.params_problem = params_problem

    def get_identifiers(self) -> Set[str]:
        return self._collect_from_formulas(lambda f: f.get_identifiers())

    def get_parameters(self) -> Set[str]:
        return self._collect_from_formulas(lambda f: f.get_parameters())

    T = TypeVar('T')

    def _collect_from_formulas(self, selector: Callable[[Formula], Iterable[T]]) -> Set[T]:
        def collect(formulas: List[Formula]):
            return {t for formula in formulas for t in selector(formula)}

        return collect(self.requirements).union(collect(self.provisions))
