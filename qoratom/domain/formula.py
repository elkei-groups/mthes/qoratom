from typing import List, Set

from .clause import Clause


class Formula:
    def __init__(self, clauses: List[Clause]):
        self.clauses: List[Clause] = clauses

    def get_identifiers(self) -> Set[str]:
        return {identifier for clause in self.clauses for identifier in clause.get_identifiers()}

    def get_parameters(self) -> Set[str]:
        return {param for clause in self.clauses for param in clause.get_parameters()}

    def __str__(self):
        return " ∨ ".join([str(clause) for clause in self.clauses])
