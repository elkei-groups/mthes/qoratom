from typing import Set, Dict


class Coefficient:

    def __init__(self, const: float = 1, params=None):
        if params is None:
            params = []
        self.const = const
        self.params = params

    def get_distinct_parameters(self) -> Set[str]:
        return {param for param in self.params}

    def __neg__(self):
        return Coefficient(-self.const, self.params)

    def __mul__(self, other):
        if isinstance(other, Coefficient):
            return Coefficient(self.const * other.const, self.params + other.params)
        elif isinstance(other, float):
            return Coefficient(self.const * other, self.params)
        elif isinstance(other, str):
            return Coefficient(self.const, self.params + [other])
        else:
            raise NotImplementedError(f"Cannot multiply coefficient with: {other}")

    def evaluate(self, param_values: Dict[str, float]) -> float:
        value = self.const
        for param in self.params:
            value *= param_values[param]
        return value

    def __str__(self):
        return " * ".join([str(self.const)] + self.params)
