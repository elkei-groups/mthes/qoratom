from enum import Enum


class Relation(Enum):
    EQ = 1
    LT = 1
    LEQ = 2
    GT = 3
    GEQ = 4

    def __str__(self):
        return {
            Relation.EQ: "==",
            Relation.LT: "<",
            Relation.LEQ: "≤",
            Relation.GT: ">",
            Relation.GEQ: "≥"
        }[self]
