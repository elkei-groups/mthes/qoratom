from typing import List, Set, Dict

from .flat.flat_clause import FlatClause
from .literal import Literal


class Clause:
    def __init__(self, literals: List[Literal]):
        self.literals: List[Literal] = literals

    def get_identifiers(self) -> Set[str]:
        return {
            identifier
            for literal in self.literals
            for identifier in literal.get_identifiers()
        }

    def get_parameters(self):
        return {
            param
            for literal in self.literals
            for param in literal.get_parameters()
        }

    def flatten(self, param_values: Dict[str, float]) -> FlatClause:
        return FlatClause([flat_clause for literal in self.literals for flat_clause in literal.flatten(param_values)])

    def __str__(self):
        return " ∧ ".join([str(literal) for literal in self.literals])
