import argparse
import os
from pathlib import Path

from qoratom.minizinc_executor import MinizincExecutor
from qoratom.ranking_executor import RankingExecutor
from qoratom.ranking_transformer import RankingTransformer


def run():
    args = _parse_args()
    with MinizincExecutor(args.minizinc, args.mznargs, args.debug) as mzn_exec, \
            RankingExecutor(mzn_exec, args.polyrank, args.debug) as ranker:
        RankingTransformer(args.infile, args.outfile, ranker).transform()


def _parse_args():
    parser = argparse.ArgumentParser(description='Replace QoS contracts in MiniZinc files by the corresponding ranking')
    parser.add_argument('infile', type=str, help='path to the input file')
    parser.add_argument('outfile', nargs='?',
                        help='path to the output file (default: input file suffixed with "_out")')
    parser.add_argument('--polyrank', type=str, help='command for executing the polytope ranking tool',
                        default="polyrank", nargs='?')
    parser.add_argument('--minizinc', type=str, help='command for executing MiniZinc', default="minizinc", nargs='?')
    parser.add_argument('--mznargs', type=str, help='arguments passed to MiniZinc ("--solver cbc" by default)',
                        nargs='?')
    parser.add_argument('--debug', action='store_true',
                        help='activate debug mode (e.g. outputs MiniZinc solution, keeps intermediate files)')
    args = parser.parse_args()
    if args.outfile is None:
        infile = Path(args.infile)
        args.outfile = os.path.join(infile.parent, infile.stem + "_out" + infile.suffix)
    return args
