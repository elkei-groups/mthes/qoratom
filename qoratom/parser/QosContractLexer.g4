lexer grammar QosContractLexer;
/*
 * Lexer Rules
 */

// fragments
fragment DIGIT : [0-9];
fragment LOWERCASE : [a-z] ;
fragment UPPERCASE : [A-Z] ;
fragment LETTER : LOWERCASE | UPPERCASE;
fragment NEWLINE : ('\r'? '\n' | '\r');

// keywords
RANKS : [cC][oO][nN][tT][rR][aA][cC][tT][rR][aA][nN][kK][sS];
REQ : [rR][eE][qQ][uU][iI][rR][eE][mM][eE][nN][tT][sS]?;
PROV : [pP][rR][oO][vV][iI][sS][iI][oO][nN][sS]?;
PARAMS : [pP][aA][rR][aA][mM][sS]?;

// non-keyword bigger words
PARAM : '_' ID;
ID : LETTER (DIGIT | LETTER | '_')*;
NUMBER : '-'? DIGIT+ ([.,] DIGIT+)?;

// "single" characters
LAND : '∧' | '/\\' | '^';
LOR : '∨' | '\\/';
TOP : '⊤';
BOT : '⊥';
LEQ : '≤' | '<=';
LT : '<';
GEQ : '≥' | '>=';
GT : '>';
IS : '==' | '=';
ZERO : '0';
PLUS : '+';
MINUS : '-';
TIMES : '*' | '×';
LBRACKET : '[';
RBRACKET : ']';
LBRACE : '{';
RBRACE : '}';
COMMA : ',';
COLON : ':';
SEMICOLON : ';';

// skipped tokens
WHITESPACE : (' ' | '\t' | NEWLINE)+ -> channel(HIDDEN);
SINGLELINECOMMENTS : '%' ~('\r' | '\n')* -> channel(HIDDEN);

// match rest also
STUFF : .+?;