from typing import TextIO

from antlr4 import CommonTokenStream

from qoratom.parser.generated.QosContractParser import QosContractParser
from qoratom.parser.generated.QosContractParserListener import QosContractParserListener as BaseListener
from qoratom.parser.to_domain_visitor import ToDomainVisitor
from qoratom.ranking_executor import RankingExecutor


class RankingExecutingListener(BaseListener):

    def __init__(self, output: TextIO, token_stream: CommonTokenStream, ranking_executor: RankingExecutor):
        self.output = output
        self.token_stream = token_stream
        self.ranking_executor = ranking_executor
        self.next_token_index = 0

    def enterRanking(self, ctx: QosContractParser.RankingContext):
        self.output.write(self.token_stream.getText(self.next_token_index, ctx.start.tokenIndex - 1))
        self.output.write("[| ")
        padding = ' ' * (ctx.start.column + 1)

        ranking = ToDomainVisitor(self.token_stream).visitRanking(ctx)
        results = self.ranking_executor.execute(ranking)

        results_string = ("\n" + padding + "| ").join([
            ", ".join([
                f"{results[rq][pr]}" for pr in ranking.provisions
            ]) for rq in ranking.requirements
        ])
        self.output.write(results_string)
        self.output.write(" |]")

    def exitRanking(self, ctx: QosContractParser.RankingContext):
        self.next_token_index = ctx.stop.tokenIndex + 1

    def exitParseRankings(self, ctx: QosContractParser.ParseRankingsContext):
        self.output.write(self.token_stream.getText(self.next_token_index))
