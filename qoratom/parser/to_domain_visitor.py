from typing import List

from antlr4 import TokenStream

from qoratom.domain.clause import Clause
from qoratom.domain.coefficient import Coefficient
from qoratom.domain.constraint import Constraint
from qoratom.domain.expression import Expression
from qoratom.domain.formula import Formula
from qoratom.domain.literal import Literal
from qoratom.domain.ranking import Ranking
from qoratom.domain.relation import Relation
from qoratom.domain.summand import Summand

from qoratom.parser.generated.QosContractParser import QosContractParser
from qoratom.parser.generated.QosContractParserVisitor import QosContractParserVisitor as BaseVisitor, TerminalNode


class ToDomainVisitor(BaseVisitor):
    def __init__(self, token_stream: TokenStream):
        self.token_stream = token_stream

    def visitRanking(self, ctx: QosContractParser.RankingContext) -> Ranking:
        provisions: List[Formula] = self.visit(ctx.provisions())
        requirements: List[Formula] = self.visit(ctx.requirements())
        params = self.visit(ctx.params()) if ctx.params() is not None else None
        return Ranking(requirements, provisions, params)

    def visitRequirements(self, ctx: QosContractParser.RequirementsContext) -> List[Formula]:
        return self.visit(ctx.formulas())

    def visitProvisions(self, ctx: QosContractParser.ProvisionsContext) -> List[Formula]:
        return self.visit(ctx.formulas())

    def visitFormulas(self, ctx: QosContractParser.FormulasContext) -> List[Formula]:
        return [self.visit(formulaCtx) for formulaCtx in ctx.formula()]

    # Visit a parse tree produced by QosContractParser#formula.
    def visitFormula(self, ctx: QosContractParser.FormulaContext) -> Formula:
        clauses: List[Clause] = [self.visit(clauseCtx) for clauseCtx in ctx.clause()]
        return Formula(clauses)

    # Visit a parse tree produced by QosContractParser#clause.
    def visitClause(self, ctx: QosContractParser.ClauseContext) -> Clause:
        literals: List[Literal] = [literal for literalCtx in ctx.literal() for literal in self.visit(literalCtx)]
        return Clause(literals)

    # Visit a parse tree produced by QosContractParser#literal.
    def visitLiteral(self, ctx: QosContractParser.LiteralContext) -> List[Literal]:
        return self.visit(ctx.constraint())

    # Visit a parse tree produced by QosContractParser#constraint.
    def visitConstraint(self, ctx: QosContractParser.ConstraintContext) -> List[Constraint]:
        expressions: List[Expression] = [self.visit(exprCtx) for exprCtx in ctx.expression()]
        relations: List[Relation] = [self.visit(relationCtx) for relationCtx in ctx.relation()]
        assert len(expressions) == len(relations) + 1
        binary_constraints = zip(expressions[0:-1], relations, expressions[1:])
        return [Constraint(lexpr, rel, rexpr) for (lexpr, rel, rexpr) in binary_constraints]

    # Visit a parse tree produced by QosContractParser#relation.
    def visitRelation(self, ctx: QosContractParser.RelationContext) -> Relation:
        if ctx.IS():
            return Relation.EQ
        if ctx.LEQ():
            return Relation.LEQ
        if ctx.GEQ():
            return Relation.GEQ
        raise Exception("Unexpected relation %s", ctx.getText())

    # Visit a parse tree produced by QosContractParser#expression.
    def visitExpression(self, ctx: QosContractParser.ExpressionContext) -> Expression:
        summands: List[Summand] = [self.visit(summandCtx) for summandCtx in ctx.summand()]
        signs: List[TerminalNode] = ctx.PLUS() + ctx.MINUS()
        signs.sort(key=lambda tn: tn.symbol.start)
        for (sign, summand) in zip(signs, summands[1:]):
            if sign.symbol.type == QosContractParser.MINUS:
                summand.invert()
        return Expression(summands)

    def visitSummand(self, ctx: QosContractParser.SummandContext) -> Summand:
        coefficient: Coefficient = Coefficient(1)
        for coefficientCtx in ctx.coefficient():
            coefficient *= self.visit(coefficientCtx)
        identifier = ctx.ID().getText() if ctx.ID() is not None else None
        return Summand(coefficient, identifier)

    def visitCoefficient(self, ctx: QosContractParser.CoefficientContext) -> Coefficient:
        coefficient = Coefficient()
        for coefficientCtx in ctx.coefficient():
            coefficient *= self.visit(coefficientCtx)
        number = ctx.NUMBER()
        if number is not None:
            coefficient *= float(number.getText())
        param = ctx.PARAM()
        if param is not None:
            coefficient *= param.getText()[1:]
        return coefficient

    def visitParams(self, ctx: QosContractParser.ParamsContext) -> str:
        start_token: int = ctx.LBRACE().symbol.tokenIndex + 1
        end_token: int = ctx.RBRACE().symbol.tokenIndex - 1
        return self.token_stream.getText(start_token, end_token).strip()
