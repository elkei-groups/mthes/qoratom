parser grammar QosContractParser;
options { tokenVocab=QosContractLexer; }

/*
 * Parser Rules
 */

parseFormula : formula EOF;
parseRankings : rankings EOF;

rankings : .*? (ranking .*?)*;
ranking : RANKS LBRACE (
        params (requirements provisions | provisions requirements) |
        (requirements params provisions) | (provisions params requirements) |
        (requirements provisions | provisions requirements) params?
    ) RBRACE;
params : PARAMS COLON LBRACE .+? RBRACE SEMICOLON;
requirements : REQ COLON formulas SEMICOLON;
provisions: PROV COLON formulas SEMICOLON;
formulas : LBRACKET formula (COMMA formula)* COMMA? RBRACKET;
formula : (clause LOR )* clause;
clause :  (literal LAND)* literal;
literal : constraint; // in theory, also (NOT? p), top and bot
constraint : expression (relation expression)+;
relation : LEQ | GEQ | IS; // in theory, also GT and LT
expression : summand (sign=(PLUS | MINUS) summand)*;
summand : (coefficient TIMES?)? ID (TIMES coefficient)? | coefficient;
coefficient : coefficient TIMES coefficient | NUMBER | PARAM;

