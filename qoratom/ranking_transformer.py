from antlr4 import CommonTokenStream, FileStream, ParseTreeWalker

from qoratom.parser.generated.QosContractLexer import QosContractLexer
from qoratom.parser.generated.QosContractParser import QosContractParser
from qoratom.parser.ranking_executing_listener import RankingExecutingListener

from qoratom.ranking_executor import RankingExecutor


class RankingTransformer:
    def __init__(self, infile_name: str, outfile_name: str, ranking_exec: RankingExecutor):
        self.infile_name: str = infile_name
        self.outfile_name: str = outfile_name
        self.ranking_exec = ranking_exec

    def transform(self) -> None:
        stream = FileStream(self.infile_name, "UTF-8")
        token_stream = CommonTokenStream(QosContractLexer(stream))
        tree = QosContractParser(token_stream).parseRankings()
        with open(self.outfile_name, "w", encoding="UTF-8") as outfile:
            listener = RankingExecutingListener(outfile, token_stream, self.ranking_exec)
            ParseTreeWalker().walk(listener, tree)
